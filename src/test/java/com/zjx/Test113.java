package com.zjx;

import java.util.ArrayList;
import java.util.List;

public class Test113 {
    public static void main(String[] args) {
        int n = 5;
        System.out.println(generate(n));
    }

    public static List<List<Integer>> generate(int numRows) {

       // int[][] arr = new int[5][5];
        List<List<Integer>> list= new ArrayList<>();

        int num = 1;

        for (int i = 0; i < numRows; i++) {
            List<Integer> list1 = new ArrayList<>();
            list1.add(num);
            for (int j = 1; j < i; j++) {
                list1.add(list.get(i - 1).get(j - 1) + list.get(i - 1).get(j));
            }
            if (i!=0) {
                list1.add(num);
            }
            list.add(list1);
        }


        return list;
    }
}
