package com.zjx;

public class Test117 extends Thread{
    private static final Object lock = new Object();
    private static boolean isThread1 = true;
    public Test117(String name){
        super(name);
    }

    static char num = 'A';
    @Override
    public void run(){


           // System.out.println(getName()+" " +i);
            System.out.println(getName()+" "+ num);
            num++;

    }

    public static void main(String[] args) throws Exception{

       /* new Test117("Thread00").start();
        for (int i = 0; i < 50; i++) {
            System.out.println(Thread.currentThread().getName()+ " " + i);
            if(i==10){
                Test117 thread111 = new Test117("Thread11");
                thread111.start();
                thread111.join();
                thread111.wait();
            }
        }*/
        Test117 thread111 = new Test117("thread11");
        Test117 thread112 = new Test117("thread12");
        Test117 thread113 = new Test117("thread13");
        thread111.start();
        thread111.join();
        thread112.start();
        thread112.join();
        thread113.start();
        thread113.join();
    }
}
