package com.zjx.thread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Queue {
    //队列
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue blockingQueue = new ArrayBlockingQueue(2);
        blockingQueue.put(1);
        blockingQueue.put(2);

        System.out.println(blockingQueue);
        System.out.println(blockingQueue.size());
    }
}
