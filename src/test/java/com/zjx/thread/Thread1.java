package com.zjx.thread;

public class Thread1 extends Thread{
    static int ticket = 100;
    private static final Object lock = new Object();


    @Override
    public void run() {
        synchronized (lock) {
            while (true) {
                if (ticket <= 0) {
                    System.out.println("没了");
                    return;
                }
                System.out.println(Thread.currentThread().getName() + " " + ticket);
                ticket--;
            }
        }
    }


    public static void main(String[] args) {
        Thread1 ticket = new Thread1();

        Thread thread1 = new Thread(ticket);
        Thread thread2 = new Thread(ticket);
        Thread thread3 = new Thread(ticket);
        thread1.start();
        thread2.start();
        thread3.start();
    }
}
