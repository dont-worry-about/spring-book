package com.zjx.thread;

import java.util.concurrent.locks.ReentrantLock;

public class Thread2 extends Thread {
    private static final Object lock = new Object();
    ReentrantLock lock1 = new ReentrantLock();
    Account account;
    Double money;

    public Thread2(String name,Account account, Double money) {
        super(name);
        this.account = account;
        this.money = money;
    }

    @Override
    public void run() {
        synchronized (account) {

            if (money > account.balance) {
                System.out.println("不够");
                return;
            }
            System.out.println(getName() + " " + "取钱");
            account.balance = account.balance - money;
            System.out.println("剩余" + account.balance);

        } }
      /*  lock1.lock();
        System.out.println(getName() + " " + "取钱");
        if (money > account.balance) {
            System.out.println("余额：" + account.balance + "余额不足");
            return;
        }
        account.balance = account.balance - money;
        System.out.println("余额：" + account.balance);
        lock1.unlock();

        }*/



    public static void main(String[] args) {
        Account account1 = new Account("111",999.6);
        new Thread2("lisi",account1,666.6).start();
     //   new Thread2("wangwu",account1,111.0).start();
        new Thread2("zhangsan",account1,666.6).start();
    }

}

class Account {
    String accountId;
    Double balance;

    public Account(String accountId, Double balance) {
        this.accountId = accountId;
        this.balance = balance;
    }

}
