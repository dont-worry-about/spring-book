package com.zjx.thread;

import jakarta.validation.constraints.Max;

import java.util.ArrayList;
import java.util.List;

public class Thread3 {
// notify

}

//自定义容器 唤起操作 容器达到某一个值
class MyList {
    private List<String> list = new ArrayList<>();

    public void add(String str) {
        list.add(str);
    }

    public int size() {
        return list.size();
    }
}

class Thread100 extends Thread {
    MyList myList;
    Object obj;

    public Thread100(MyList myList, Object obj) {
        this.myList = myList;
        this.obj = obj;
    }

    @Override
    public void run() {

        if (myList.size() < 5) {
            try {
                System.out.println(getName() + "长度是5");
                obj.wait();
                System.out.println(getName() + "wait唤醒");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Thread200 extends Thread {
    MyList myList;
    Object obj;

    public Thread200(MyList myList, Object obj) {
        this.myList = myList;
        this.obj = obj;
    }

    @Override
    public void run() {
        try {
            synchronized (obj) {
                for (int i = 0; i < 10; i++) {
                    myList.add(i + "");
                    if (myList.size() == 5) {
                        System.out.println("通知");
                        obj.notify();
                    }
                    Thread.sleep(1000);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}