package com.zjx;

import java.util.Arrays;

public class Test116 {

    public static void quickSort(int[] nums) {
        quickSort(nums, 0, nums.length - 1);
    }

    public static void quickSort(int[] nums, int left, int right) {
        if (left >= right) {
            return;
        }
        int base = nums[left];
        int l = left, r = right;
        while (l < r) {
            while (nums[r] >= base && l < r) {
                r--;
            }
            nums[l] = nums[r];
            while (nums[l] <= base && l < r) {
                l++;
            }
            nums[r] = nums[l];
        }

        nums[l] = base;
        System.out.println(Arrays.toString(nums));
        quickSort(nums, left, l - 1);
        quickSort(nums, l + 1, right);
    }

    public static void main(String[] args) {

        int[] nums = new int[]{3, 1, 2, 5, 4, 6, 9, 7, 10, 8, 0};
        quickSort(nums);
        System.out.println(Arrays.toString(nums));
    }
}

