package com.zjx.container;

import java.util.LinkedList;

public class Container {
    private LinkedList<Integer> list = new LinkedList<>();
    // 队列最大容量
    private int capacity = 10;

    public void put(int value) {
        synchronized (this) {
            // 容器满了
            while (list.size() == capacity) {
                System.out.println("full waiting");
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            // 如果没有满 继续添加元素
            list.offer(value);
            System.out.println(Thread.currentThread().getName() + " put " + value);
            // 如果之前队列为空 会有消费者等待数据
            // 在添加数据后  需要唤醒可能等待的消费者 （空的状态 变为 非空状态）
            this.notifyAll();
        }
    }

    public Integer take() {
        Integer value = 0;
        synchronized (this) {
            // 判断容器中是否存在数据
            // 如果为空 等待
            while (list.size() == 0) {
                System.out.println("empty waiting");
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            // 如果不为空 取出队首元素
            value = list.poll();
            System.out.println(Thread.currentThread().getName() + " take " + value);
            // 取数据后 可能让队列从满的状态 变为不满的状态
            // 唤醒可能等待的生产者
            this.notifyAll();
        }
        return value;
    }

}
