package com.zjx.container;

import java.util.LinkedList;

public class Container1 {
    private LinkedList<Integer> list = new LinkedList<>();
    private int capacity = 10;

    public void put(int value) throws InterruptedException {
        synchronized (this) {
            while (list.size() == capacity);{
                this.wait();
            }

        }
        //如果没满  继续添加队列
        list.offer(value);
        System.out.println(Thread.currentThread().getName() + "put"+ value);
        //如果之前队列为空  会有消费者等待
        this.notifyAll();


    }
    public Integer take() throws InterruptedException {
        Integer value = 0;
        //判断容器是否存在数据
        synchronized (this) {
            if (list.size() == 0) {
                System.out.println("空");
                this.wait();
            }
            value = list.poll();
            System.out.println(Thread.currentThread().getName()+"take"+ value);
        }
        return value;
    }
}
