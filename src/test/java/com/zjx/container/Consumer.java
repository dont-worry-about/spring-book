package com.zjx.container;

public class Consumer implements Runnable{
    Container container;

    public Consumer(Container container) {
        this.container = container;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

                Integer value = container.take();

        }
    }
}
