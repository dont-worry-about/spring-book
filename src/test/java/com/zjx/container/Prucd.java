package com.zjx.container;

import lombok.SneakyThrows;

import java.util.Random;

public class Prucd implements Runnable{
    Container container;
   // Object object = new Object();
    public Prucd(Container container){
        this.container = container;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

                container.put(new Random().nextInt(100));

        }
    }
}
