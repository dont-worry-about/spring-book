package com.zjx;

public class Test114 {


    public static void main(String[] args) {
        int[] height = {1, 8, 6, 2, 5, 4, 8, 3, 7};
        System.out.println(maxArea(height));
    }

    public static int maxArea(int[] height) {
        int max = 0;
        /*
        for (int i = 0; i < height.length-1; i++) {
            for (int j = i+1; j < height.length; j++) {
                int num = 0;
                if(height[i] >= height[j]){
                     num = (j-i)*height[j];
                //    System.out.println(i+1);
                }else {
                     num = (j-i)*height[i];
                  //  System.out.println(num);
                }
                if(max < num){
                    max = num;
                }

            }
        }
*/
        int l = 0;
        int r = height.length - 1;
        while (l != r) {
            max = Math.max(max, Math.min(height[l], height[r]) * (r - l));
            if (height[l] >= height[r]) {
                r = r - 1;
            } else {
                l = l + 1;
            }


        }
        return max;
    }
}
