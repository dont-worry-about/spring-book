package com.zjx;

import java.util.ArrayList;
import java.util.List;

public class Test124 {
    public static void main(String[] args) {
        System.out.println(fizzBuzz(3));

    }
    public static List<String> fizzBuzz(int n) {
        List list = new ArrayList();
        for (int i = 1; i <= n; i++) {
            if(i % 3==0 && i % 5==0){
                list.add("FizzBuzz");
                continue;
            }
            if(i % 3==0 ){
                list.add("Fizz");
                continue;
            }
            if(i % 5==0){
                list.add("Buzz");
                continue;
            }
            String s = String.valueOf(i);
            list.add(s);

        }

        return list;
    }
}
