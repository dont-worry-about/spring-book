package com.zjx;

import java.util.Arrays;

public class Test111 {
    public static void main(String[] args) {
//        1365. 有多少小于当前数字的数字
     int[] nums = {8,1,2,2,3};
        System.out.println(Arrays.toString(smallerNumbersThanCurrent(nums)));
    }
    public static int[] smallerNumbersThanCurrent(int[] nums) {
        int[] numb = new int[nums.length];

        for (int i = 0; i < nums.length; i++) {
            int ss = 0;
            for (int j = 0; j < nums.length; j++) {
                if(i!=j) {
                    if (nums[i] > nums[j]) {
                        ss++;
                    }
                }
            }
            numb[i] = ss;
        }
        return numb;
    }
}
