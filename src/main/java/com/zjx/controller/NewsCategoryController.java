package com.zjx.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 新闻类别 前端控制器
 * </p>
 *
 * @author zjx
 * @since 2024-02-11
 */
@Controller
@RequestMapping("/newsCategory")
public class NewsCategoryController {

}
