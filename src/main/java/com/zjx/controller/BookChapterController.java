package com.zjx.controller;

import com.zjx.dto.ChapterInfoRespDto;
import com.zjx.dto.SearchBookRespDto;
import com.zjx.dto.SearchBookWorkRespDto;
import com.zjx.resp.RestResp;
import com.zjx.service.IBookChapterService;
import com.zjx.service.impl.BookChapterServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 小说章节 前端控制器
 * </p>
 *
 * @author zjx
 * @since 2024-01-30
 */
@CrossOrigin(originPatterns = "*", allowCredentials = "true")
@RestController
@RequestMapping("/api/front")
public class BookChapterController {
    @Autowired
    private IBookChapterService bookChapterService;

    //http://127.0.0.1:8888/api/front/book/chapter/list?bookId=1334335471568912384
    @Operation(summary = "章节列表")
    @GetMapping("/book/chapter/list")
    public RestResp<List<ChapterInfoRespDto>> chapter(Long bookId) {
        List<ChapterInfoRespDto> chapterInfoRespDtos = bookChapterService.chapterByid(bookId);
        return RestResp.ok(chapterInfoRespDtos);
    }
    @Operation(summary = "分类查询")
    @GetMapping("/book/category/list")
    public RestResp<List<SearchBookWorkRespDto>> seachwork(@RequestParam(defaultValue = "0") String workDirection) {
        List<SearchBookWorkRespDto> searchBookWorkRespDtos = bookChapterService.searchWork(workDirection);
        return RestResp.ok(searchBookWorkRespDtos);
    }

    @Operation(summary = "查询")
    @GetMapping("/search/books")
    public RestResp<List<SearchBookRespDto>> serch(@RequestParam(required = false) String keyword,
                                                   @RequestParam(required = false) String workDirection,
                                                   @RequestParam(required = false) String categoryId,
                                                   @RequestParam(required = false, defaultValue = "0") String isVip,
                                                   @RequestParam(required = false, defaultValue = "0") String bookStatus,
                                                   @RequestParam(required = false)  String wordCountMin,
                                                   @RequestParam(required = false)  String wordCountMax,
                                                   @RequestParam(required = false)  String updateTimeMin,
                                                   @RequestParam(required = false)  String sort,
                                                   @RequestParam(defaultValue = "1") String pageNum,
                                                   @RequestParam(defaultValue = "10") String pageSize) {
        List<SearchBookRespDto> searchBookRespDtos = bookChapterService.searchBook(keyword, workDirection, categoryId, isVip, bookStatus, wordCountMin, wordCountMax, updateTimeMin, sort, pageNum, pageSize);
        return RestResp.ok(searchBookRespDtos);

    }


}
