package com.zjx.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 友情链接 前端控制器
 * </p>
 *
 * @author zjx
 * @since 2024-02-17
 */
@Controller
@RequestMapping("/homeFriendLink")
public class HomeFriendLinkController {

}
