package com.zjx.controller;

import com.zjx.dto.FriendLinkRespDto;
import com.zjx.dto.HomeBookRespDto;
import com.zjx.resp.RestResp;
import com.zjx.service.IHomeBookService;
import com.zjx.service.IHomeFriendLinkService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 小说推荐 前端控制器
 * </p>
 *
 * @author zjx
 * @since 2024-01-29
 */
@CrossOrigin(originPatterns = "*",allowCredentials = "true")
@RestController
@RequestMapping("/api/front/home")
public class HomeBookController {
    @Autowired
    private IHomeBookService homeBookService;

    @Autowired
    private IHomeFriendLinkService friendLinkService;
    // http://127.0.0.1:8888/api/front/home/books
    @GetMapping("/books")
    public RestResp<List<HomeBookRespDto>> books() {
        // json结构
        // code  message  data  ok  -> RestResp
        //    [{type:"",bookId:"",...},{type:"",bookId:"",...}]
        List<HomeBookRespDto> list = homeBookService.books();
        // 都取出来 批量查询
        return RestResp.ok(list);
    }

    //http://127.0.0.1:8888/api/front/home/friend_Link/list
    @Operation(summary = "链接")
   @GetMapping("/friend_Link/list")
    public RestResp<List<FriendLinkRespDto>> link() {
       List<FriendLinkRespDto> link = friendLinkService.link();
       return RestResp.ok(link);
    }

}


