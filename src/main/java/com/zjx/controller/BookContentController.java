package com.zjx.controller;

import com.zjx.dto.ChapterContentRespDto;
import com.zjx.resp.RestResp;
import com.zjx.service.IBookContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 小说内容 前端控制器
 * </p>
 *
 * @author zjx
 * @since 2024-01-31
 */
@CrossOrigin(originPatterns = "*", allowCredentials = "true")
@RestController
@RequestMapping("/api/front/book")
public class BookContentController {
    @Autowired
    IBookContentService bookContentService;

    @GetMapping("/content/{chapterId}")
    public RestResp<ChapterContentRespDto> content(@PathVariable("chapterId") Long chapterId) {
        ChapterContentRespDto content = bookContentService.content(chapterId);
        return RestResp.ok(content);

    }

    @GetMapping("/next_chapter_id/{chapterId}")
    public RestResp<Long> pages(@PathVariable("chapterId") Long chapterId){


        return bookContentService.contentNext(chapterId);
    }
    @GetMapping("/pre_chapter_id/{chapterId}")
    public RestResp<Long> pagePre(@PathVariable("chapterId") Long chapterId){


        return bookContentService.contentPre(chapterId);
    }

}
