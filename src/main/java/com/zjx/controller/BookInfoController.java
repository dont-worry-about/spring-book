package com.zjx.controller;

import com.zjx.dto.BookInfoRespDto;

import com.zjx.dto.LastChapterRespDto;
import com.zjx.dto.RankBookRespDto;
import com.zjx.dto.req.BookVisitReqDto;
import com.zjx.mapper.BookInfoMapper;
import com.zjx.po.BookInfo;
import com.zjx.resp.RestResp;
import com.zjx.service.IBookInfoService;

import io.swagger.v3.oas.annotations.Operation;
import org.mybatis.logging.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.logging.Logger;

/**
 * <p>
 * 小说信息 前端控制器
 * </p>
 *
 * @author zjx
 * @since 2024-01-29
 */
@CrossOrigin(originPatterns = "*", allowCredentials = "true")
@RestController
@RequestMapping("/api/front/book")
public class BookInfoController {

    @Autowired
    private IBookInfoService bookInfoService;
    @Autowired
    private BookInfoMapper bookInfoMapper;



    // http://127.0.0.1:8888/api/front/book/{id}
    @Operation(summary = "小说信息查询接口")
    @GetMapping("/{id}")
    public RestResp<BookInfoRespDto> info(@PathVariable("id") Long id) {
        // 增加入参的打印
        System.out.println(id);

       /* BookInfo bookInfo = bookInfoService.getById(id);
        System.out.println(bookInfo);
        BookInfoRespDto dto = BookInfoRespDto.builder().build();*/


//                .id(id)
//                .categoryId(bookInfo.getCategoryId())
//                .categoryName(bookInfo.getCategoryName())
//                .picUrl(bookInfo.getPicUrl())
//                .bookName(bookInfo.getBookName())
//                .authorId(bookInfo.getAuthorId())
//                .authorName(bookInfo.getAuthorName())
//                .bookDesc(bookInfo.getBookDesc())
//                .bookStatus(bookInfo.getBookStatus())
//                .visitCount(bookInfo.getVisitCount())
//                .wordCount(bookInfo.getWordCount())
//                .commentCount(bookInfo.getCommentCount())
//                .lastChapterId(bookInfo.getLastChapterId())
//                .lastChapterName(bookInfo.getLastChapterName())
//                .updateTime(bookInfo.getLastChapterUpdateTime())
//                .build();
        // 当数据库中的字段 和 要返回的dto中的字段 几乎一致时
        // 可以使用这个工具类  进行属性的拷贝  这里是将bookInfo中的属性值 拷贝到dto中
        // 遍历bookInfo中的所有属性  然后找到和dto中的同名属性  进行赋值
       /* BeanUtils.copyProperties(bookInfo,dto);*

        */

        BookInfoRespDto bookInfoRespDto = bookInfoService.bookInfoResp(id);
        return RestResp.ok(bookInfoRespDto);
    }

    @Operation(summary = "最新章节")
    @GetMapping("/last_chapter/about")
    public RestResp<LastChapterRespDto> chapterInfo(Long bookId){

        LastChapterRespDto dto = bookInfoService.look(bookId);
        return RestResp.ok(dto);
    }

    @Operation(summary = "同类推荐")
    @GetMapping("/rec_list")
    public RestResp<List<BookInfoRespDto>> recInfo(@RequestParam("bookId") Long bookId){

        List<BookInfoRespDto> bookInfoRespDtos = bookInfoService.bookRecResp(bookId);
        return RestResp.ok(bookInfoRespDtos);
    }

    @Operation(summary = "点击榜")
    @GetMapping("/visit_rank")
    public RestResp<List<RankBookRespDto>> visit(){
        List<RankBookRespDto> list = bookInfoService.visit();
        return RestResp.ok(list);
    }

    @Operation(summary = "新书榜")
    @GetMapping("/newest_rank")
    public RestResp<List<RankBookRespDto>> newest(){
        List<RankBookRespDto> list = bookInfoService.newest();
        return RestResp.ok(list);
    }


    @Operation(summary = "更新榜")
    @GetMapping("/update_rank")
    public RestResp<List<RankBookRespDto>> update(){
        List<RankBookRespDto> list = bookInfoService.updaterank();
        return RestResp.ok(list);
    }

    @Operation(summary = "点击量")
    @PostMapping("/visit")
    public RestResp<Void> visit(@RequestBody BookVisitReqDto reqDto){


       // bookInfoService.visitBook(id);
       // bookInfoService.visitBook(id);
        //return "Cg";
        bookInfoMapper.updateClickCount(reqDto.getBookId());
        return RestResp.ok(null);


    }
}