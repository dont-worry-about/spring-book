package com.zjx.controller;

import com.zjx.dto.UserLoginRespDto;
import com.zjx.dto.UserRegisterRespDto;
import com.zjx.dto.VerifyCodeRespDto;
import com.zjx.dto.req.UserLoginReqDto;
import com.zjx.dto.req.UserRegisterReqDto;
import com.zjx.resp.RestResp;
import com.zjx.service.IUserInfoService;
import org.mybatis.logging.Logger;
import org.mybatis.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/front")
public class UserInfoController {

    private Logger logger = LoggerFactory.getLogger(UserInfoController.class);

    @Autowired
    private IUserInfoService userInfoService;

    @GetMapping("/resource/img_verify_code")
    public RestResp<VerifyCodeRespDto> imgVerifyCode() {
        //logger.info("方法名是:{}", "imgVerifyCode");
        VerifyCodeRespDto verifyCodeRespDto = userInfoService.imgVerifyCode();
       // logger.info("方法名是:{}, 返回结果是:{}", "imgVerifyCode", verifyCodeRespDto);
        return RestResp.ok(verifyCodeRespDto);
    }

    @PostMapping("/user/register")
    public RestResp<UserRegisterRespDto> register(@RequestBody UserRegisterReqDto reqDto) {
        //logger.info("方法名是:{}, 参数是:{}", "register", reqDto);
        UserRegisterRespDto respDto = userInfoService.register(reqDto);
        if (respDto.getType() == 1) {
            return RestResp.fail("00001", "验证码错误啦！", respDto);
        }
        if (respDto.getType() == 2) {
            return RestResp.fail("00002", "用户名已经被使用啦！", respDto);
        }
      //  logger.info("方法名是:{}, 参数是:{}, 返回结果是:{}", "register", reqDto, respDto);
        return RestResp.ok(respDto);
    }

    @PostMapping("/user/login")
    public RestResp<UserLoginRespDto> login(@RequestBody UserLoginReqDto reqDto) {

        UserLoginRespDto respDto = userInfoService.login(reqDto);

        if (respDto.getType() == 1) {
            return RestResp.fail("00003", "用户名不存在~", respDto);
        }
        if (respDto.getType() == 2) {
            return RestResp.fail("00004", "密码错误~", respDto);
        }
        return RestResp.ok(respDto);
    }
}
