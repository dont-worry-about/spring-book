package com.zjx.controller;

import com.zjx.dto.NewsInfoRespDto;
import com.zjx.dto.RankBookRespDto;
import com.zjx.resp.RestResp;
import com.zjx.service.INewsInfoService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 新闻信息 前端控制器
 * </p>
 *
 * @author zjx
 * @since 2024-02-11
 */
@RestController
@RequestMapping("/api/front/news")
public class NewsInfoController {
    @Autowired
    INewsInfoService newsInfoService;

    @GetMapping("/{id}")
    public RestResp<List<NewsInfoRespDto>> newsInfo(@PathVariable("id") Integer id) {
        List<NewsInfoRespDto> list = newsInfoService.newsInfoResp(id);
        return RestResp.ok(list);
    }
    @Operation(summary = "新闻")

 @GetMapping("/latest_list")
 //http://127.0.0.1:8888/api/front/news/latest_list
    public RestResp<List<NewsInfoRespDto>> newsInfos() {
       // List<NewsInfoRespDto> list = newsInfoService.newsInfosResp(id);
        List<NewsInfoRespDto> list = newsInfoService.newsInfosResp();
        return RestResp.ok(list);
    }

}
