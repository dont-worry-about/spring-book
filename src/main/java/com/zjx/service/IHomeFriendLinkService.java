package com.zjx.service;

import com.zjx.dto.FriendLinkRespDto;
import com.zjx.po.HomeFriendLink;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 友情链接 服务类
 * </p>
 *
 * @author zjx
 * @since 2024-02-17
 */
public interface IHomeFriendLinkService extends IService<HomeFriendLink> {
    List<FriendLinkRespDto> link();
}
