package com.zjx.service;

import com.zjx.dto.HomeBookRespDto;
import com.zjx.po.HomeBook;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 小说推荐 服务类
 * </p>
 *
 * @author zjx
 * @since 2024-01-29
 */
public interface IHomeBookService extends IService<HomeBook> {

    List<HomeBookRespDto> books();
    List<HomeBookRespDto> allBooks();
}
