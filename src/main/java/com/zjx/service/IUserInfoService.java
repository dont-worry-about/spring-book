package com.zjx.service;

import com.zjx.dto.UserLoginRespDto;
import com.zjx.dto.req.UserLoginReqDto;
import com.zjx.po.UserInfo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjx.dto.UserRegisterRespDto;
import com.zjx.dto.VerifyCodeRespDto;
import com.zjx.dto.req.UserRegisterReqDto;

public interface IUserInfoService extends IService<UserInfo> {

    VerifyCodeRespDto imgVerifyCode();
    UserRegisterRespDto register(UserRegisterReqDto reqDto);

    UserLoginRespDto login(UserLoginReqDto reqDto);
}
