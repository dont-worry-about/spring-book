package com.zjx.service;

import com.zjx.po.NewsCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 新闻类别 服务类
 * </p>
 *
 * @author zjx
 * @since 2024-02-11
 */
public interface INewsCategoryService extends IService<NewsCategory> {

}
