package com.zjx.service;


import com.zjx.dto.NewsInfoRespDto;
import com.zjx.po.NewsInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * <p>
 * 新闻信息 服务类
 * </p>
 *
 * @author zjx
 * @since 2024-02-11
 */
public interface INewsInfoService extends IService<NewsInfo> {

    List<NewsInfoRespDto> newsInfoResp(@Param("id") Integer id);

    List<NewsInfoRespDto> newsInfosResp();


}
