package com.zjx.service;

import com.zjx.po.NewsContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 新闻内容 服务类
 * </p>
 *
 * @author zjx
 * @since 2024-02-11
 */
public interface INewsContentService extends IService<NewsContent> {

}
