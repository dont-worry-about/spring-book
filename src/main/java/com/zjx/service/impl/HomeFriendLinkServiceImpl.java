package com.zjx.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zjx.dto.FriendLinkRespDto;
import com.zjx.po.HomeFriendLink;
import com.zjx.mapper.HomeFriendLinkMapper;
import com.zjx.service.IHomeFriendLinkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 友情链接 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2024-02-17
 */
@Service
public class HomeFriendLinkServiceImpl extends ServiceImpl<HomeFriendLinkMapper, HomeFriendLink> implements IHomeFriendLinkService {
    @Autowired
    HomeFriendLinkMapper homeFriendLinkMapper;

    @Override
    public List<FriendLinkRespDto> link() {
        List<FriendLinkRespDto> list = new ArrayList<>();
        QueryWrapper queryWrapper = new QueryWrapper();
        List<HomeFriendLink> homeFriendLinks = homeFriendLinkMapper.selectList(null);
        for (HomeFriendLink homeFriendLink : homeFriendLinks) {
            FriendLinkRespDto dto = FriendLinkRespDto.builder()
                    .linkName(homeFriendLink.getLinkName())
                    .linkUrl(homeFriendLink.getLinkUrl()).
                     build();
            list.add(dto);
        }

        return list;
    }
}
