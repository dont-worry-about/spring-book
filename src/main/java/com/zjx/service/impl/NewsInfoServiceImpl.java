package com.zjx.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zjx.dto.NewsInfoRespDto;
import com.zjx.po.NewsInfo;
import com.zjx.mapper.NewsInfoMapper;
import com.zjx.service.INewsInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 新闻信息 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2024-02-11
 */
@Service
public class NewsInfoServiceImpl extends ServiceImpl<NewsInfoMapper, NewsInfo> implements INewsInfoService {


    @Autowired
    NewsInfoMapper newsInfoMapper;
    @Override
    public List<NewsInfoRespDto> newsInfoResp(Integer id) {

        List<NewsInfoRespDto> list = new ArrayList<>();

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("id",id);
        List list1 = newsInfoMapper.selectList(queryWrapper);

        NewsInfoRespDto dto = NewsInfoRespDto.builder().build();

        for (Object o : list1) {
            BeanUtils.copyProperties(o,dto);
            list.add(dto);
        }


        return list;
    }

    @Override
    public List<NewsInfoRespDto> newsInfosResp() {
        List<NewsInfoRespDto> list = new ArrayList<>();

        QueryWrapper<NewsInfo> queryWrapper = new QueryWrapper();
        //queryWrapper.eq("id",id);
        queryWrapper.orderByDesc("update_time").last("limit 2");

        List list1 = newsInfoMapper.selectList(queryWrapper);



        for (Object o : list1) {
            NewsInfoRespDto dto = NewsInfoRespDto.builder().build();
            BeanUtils.copyProperties(o,dto);
            list.add(dto);
        }


        return list;
    }
}
