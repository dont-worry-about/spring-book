package com.zjx.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zjx.dto.BookInfoRespDto;
import com.zjx.dto.ChapterInfoRespDto;
import com.zjx.dto.LastChapterRespDto;
import com.zjx.dto.RankBookRespDto;
import com.zjx.mapper.BookChapterMapper;
import com.zjx.mapper.BookContentMapper;
import com.zjx.po.BookChapter;
import com.zjx.po.BookContent;
import com.zjx.po.BookInfo;
import com.zjx.mapper.BookInfoMapper;
import com.zjx.resp.RestResp;
import com.zjx.service.IBookInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 小说信息 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2024-01-29
 */
@Service
public class BookInfoServiceImpl extends ServiceImpl<BookInfoMapper, BookInfo> implements IBookInfoService {
    @Autowired
    private BookInfoMapper bookInfoMapper;
    @Autowired
    private BookChapterMapper bookChapterMapper;
    @Autowired
    private BookContentMapper bookContentMapper;


    @Override
    public BookInfoRespDto bookInfoResp(Long id) {
        /*List<BookInfoRespDto> list = new ArrayList<>();

        List<BookInfo> bookInfos = bookInfoMapper.selectById(id);
*/
        BookInfo bookInfo1 = bookInfoMapper.selectById(id);


            BookInfoRespDto dto = BookInfoRespDto.builder().build();
        BeanUtils.copyProperties(bookInfo1,dto);
        dto.setUpdateTime(LocalDateTime.parse(String.valueOf(bookInfo1.getLastChapterUpdateTime())));

      /*  QueryWrapper<BookChapter> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("book_id",id).eq("chapter_num",0);
        LambdaQueryWrapper<BookChapter> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(BookChapter::getBookId,id).eq(BookChapter::getChapterNum,0);
        BookChapter bookChapter = bookChapterMapper.selectOne(queryWrapper);
        dto.setFirstChapterId(bookChapter.getId());*/
        return dto;
    }

    @Override
    public LastChapterRespDto look(Long bookld) {

        LastChapterRespDto dto = LastChapterRespDto.builder().build();
        LambdaQueryWrapper<BookChapter> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(BookChapter::getBookId,bookld)
                .orderByDesc(BookChapter::getChapterNum).last("limit 1");

        BookChapter bookChapter = bookChapterMapper.selectOne(lambdaQueryWrapper);

        ChapterInfoRespDto dto1 = ChapterInfoRespDto.builder()
                .id(bookChapter.getId())
                .bookId(bookChapter.getBookId())
                .chapterNum(bookChapter.getChapterNum())
                .chapterName(bookChapter.getChapterName())
                .chapterWordCount(bookChapter.getWordCount())
                .chapterUpdateTime(bookChapter .getUpdateTime())
                .build();
        dto.setChapterInfo(dto1);

        Integer selectCount = Math.toIntExact(bookChapterMapper.selectCount(lambdaQueryWrapper));
        dto .setChapterTotal(selectCount);

        QueryWrapper<BookContent> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("chapter_id",bookChapter.getId());
        BookContent bookContent = bookContentMapper.selectOne(queryWrapper);
        String summary = bookContent.getContent();
        String s = summary.substring(0,30);
        dto.setContenSummary(s);

        return dto;
    }

    @Override
    public List<BookInfoRespDto> bookRecResp(Long bookId) {

        List<BookInfoRespDto> list = new ArrayList<>();

        QueryWrapper queryWrapper = new QueryWrapper();
        QueryWrapper queryWrapper1 = new QueryWrapper();

        queryWrapper.eq("id",bookId);
        BookInfo bookInfo = bookInfoMapper.selectOne(queryWrapper);

        queryWrapper1.eq("category_id",bookInfo.getCategoryId());
        queryWrapper1.ne("id",bookId);
        List<BookInfo> bookInfos = bookInfoMapper.selectList(queryWrapper1);
        for (BookInfo info : bookInfos) {
            System.out.println(info);
        }
        Collections.shuffle(bookInfos);
        int num = 0;
            for (BookInfo info : bookInfos) {
                BookInfoRespDto dto = BookInfoRespDto.builder().build();
                BeanUtils.copyProperties(info,dto);
                list.add(dto);
                    num++;

                    if(num==4){
                        break;
                    }

            }


        return list;
    }


    @Override
    public List<RankBookRespDto> visit() {
        List<RankBookRespDto> list = new ArrayList<>();
        QueryWrapper<BookInfo> queryWrapper = new QueryWrapper();
        //visit_count降序排列
        queryWrapper.orderByDesc("visit_count").last("limit 50");
        List<BookInfo> bookInfoList = bookInfoMapper.selectList(queryWrapper);

        bookInfoList.forEach(bookInfo -> {
            RankBookRespDto dto1 = RankBookRespDto.builder().build();
            BeanUtils.copyProperties(bookInfo,dto1);

            list.add(dto1);

        });
        return list;
    }

    @Override
    public List<RankBookRespDto> newest() {
        List<RankBookRespDto> list = new ArrayList<>();
        QueryWrapper<BookInfo> queryWrapper = new QueryWrapper();
        queryWrapper.last("limit 50");
        List<BookInfo> bookInfoList = bookInfoMapper.selectList(queryWrapper);

        bookInfoList.forEach(bookInfo -> {
            RankBookRespDto dto1 = RankBookRespDto.builder().build();
            BeanUtils.copyProperties(bookInfo,dto1);

            list.add(dto1);

        });
        return list;
    }
    @Override
    public List<RankBookRespDto> updaterank() {
        List<RankBookRespDto> list = new ArrayList<>();
        QueryWrapper<BookInfo> queryWrapper = new QueryWrapper();
        //last_chapter_update_time降序排列
        queryWrapper.orderByDesc("last_chapter_update_time")
                .last("limit 10");
        List<BookInfo> bookInfoList = bookInfoMapper.selectList(queryWrapper);

        bookInfoList.forEach(bookInfo -> {
            RankBookRespDto dto1 = RankBookRespDto.builder().build();
            BeanUtils.copyProperties(bookInfo,dto1);

            list.add(dto1);

        });
        return list;
    }




}
