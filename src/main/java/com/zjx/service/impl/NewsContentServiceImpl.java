package com.zjx.service.impl;

import com.zjx.po.NewsContent;
import com.zjx.mapper.NewsContentMapper;
import com.zjx.service.INewsContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 新闻内容 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2024-02-11
 */
@Service
public class NewsContentServiceImpl extends ServiceImpl<NewsContentMapper, NewsContent> implements INewsContentService {

}
