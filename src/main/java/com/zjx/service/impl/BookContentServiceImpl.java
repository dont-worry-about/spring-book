package com.zjx.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zjx.dto.BookInfoRespDto;
import com.zjx.dto.ChapterContentRespDto;
import com.zjx.dto.ChapterInfoRespDto;
import com.zjx.mapper.BookChapterMapper;
import com.zjx.mapper.BookInfoMapper;
import com.zjx.po.BookChapter;
import com.zjx.po.BookContent;
import com.zjx.mapper.BookContentMapper;
import com.zjx.po.BookInfo;
import com.zjx.resp.RestResp;
import com.zjx.service.IBookContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 小说内容 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2024-01-31
 */
@Service
public class BookContentServiceImpl extends ServiceImpl<BookContentMapper, BookContent> implements IBookContentService {

    @Autowired
    BookContentMapper bookContentMapper;
    @Autowired
    BookInfoMapper bookInfoMapper;
    @Autowired
    BookChapterMapper bookChapterMapper;

    @Override
    public ChapterContentRespDto content(Long chapterId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("chapter_id",chapterId);
       // List<BookChapter> bookChapters = bookChapterMapper.selectList(queryWrapper1);
       // System.out.println(bookChapters);
        BookContent bookContent = bookContentMapper.selectOne(queryWrapper);
        System.out.println(bookContent);

        LambdaQueryWrapper<BookChapter> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(BookChapter::getId,chapterId);
        BookChapter bookChapter = bookChapterMapper.selectOne(lambdaQueryWrapper);

        BookInfo bookInfo = bookInfoMapper.selectById(bookChapter.getBookId());
        BookInfoRespDto dto2 = BookInfoRespDto.builder().build();
        BeanUtils.copyProperties(bookInfo,dto2);

        ChapterInfoRespDto dto1 = ChapterInfoRespDto.builder()
                .id(bookChapter.getId())
                .bookId(bookChapter.getBookId())
                .chapterNum(bookChapter.getChapterNum())
                .chapterName(bookChapter.getChapterName())
                .chapterWordCount(bookChapter.getWordCount())
                .chapterUpdateTime(bookChapter .getUpdateTime())
                .isVip(bookChapter.getIsVip())
                .build();

        ChapterContentRespDto dto = ChapterContentRespDto.builder()
                .bookInfo(dto2)
                .chapterInfo(dto1)
                .id(bookContent.getId())
                .chapterId(chapterId)
                .bookContent(bookContent.getContent())
                .createTime(String.valueOf(bookContent.getCreateTime()))
                .updateTime(String.valueOf(bookContent.getUpdateTime()))


                .build();


        return dto;
    }

    @Override
    public RestResp<Long> contentNext(Long chapterId) {


        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("chapter_id",chapterId);
        // List<BookChapter> bookChapters = bookChapterMapper.selectList(queryWrapper1);
        // System.out.println(bookChapters);
        BookContent bookContent = bookContentMapper.selectOne(queryWrapper);
       // System.out.println(bookContent);
        //下一章内容
        QueryWrapper queryWrapper1 = new QueryWrapper();
        queryWrapper1.eq("id",bookContent.getId()+1);
      //  BookContent bookContent1 = bookContentMapper.selectById(bookContent.getId() + 2);
       // System.out.println(bookContent1);
        BookContent bookContent1 = bookContentMapper.selectOne(queryWrapper1);
        System.out.println(bookContent1);



        LambdaQueryWrapper<BookChapter> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(BookChapter::getId,bookContent1.getChapterId());
        BookChapter bookChapter = bookChapterMapper.selectOne(lambdaQueryWrapper);

        BookInfo bookInfo = bookInfoMapper.selectById(bookChapter.getBookId());
        BookInfoRespDto dto2 = BookInfoRespDto.builder().build();
        BeanUtils.copyProperties(bookInfo,dto2);

        ChapterInfoRespDto dto1 = ChapterInfoRespDto.builder()
                .id(bookChapter.getId())
                .bookId(bookChapter.getBookId())
                .chapterNum(bookChapter.getChapterNum())
                .chapterName(bookChapter.getChapterName())
                .chapterWordCount(bookChapter.getWordCount())
                .chapterUpdateTime(bookChapter .getUpdateTime())
                .isVip(bookChapter.getIsVip())
                .build();

        ChapterContentRespDto dto = ChapterContentRespDto.builder()
                .bookInfo(dto2)
                .chapterInfo(dto1)
                .id(bookContent1.getId())
                .chapterId(bookContent1.getChapterId())
                .bookContent(bookContent1.getContent())
                .createTime(String.valueOf(bookContent1.getCreateTime()))
                .updateTime(String.valueOf(bookContent1.getUpdateTime()))


                .build();


        Long chapterId1 = bookContent1.getChapterId();

        return RestResp.ok(dto.getChapterId());
    }

    @Override
    public RestResp<Long> contentPre(Long chapterId) {

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("chapter_id",chapterId);
        // List<BookChapter> bookChapters = bookChapterMapper.selectList(queryWrapper1);
        // System.out.println(bookChapters);
        BookContent bookContent = bookContentMapper.selectOne(queryWrapper);
        // System.out.println(bookContent);
        //下一章内容
        QueryWrapper queryWrapper1 = new QueryWrapper();
        queryWrapper1.eq("id",bookContent.getId()-1);
        //  BookContent bookContent1 = bookContentMapper.selectById(bookContent.getId() + 2);
        // System.out.println(bookContent1);
        BookContent bookContent1 = bookContentMapper.selectOne(queryWrapper1);
        System.out.println(bookContent1);



        LambdaQueryWrapper<BookChapter> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(BookChapter::getId,bookContent1.getChapterId());
        BookChapter bookChapter = bookChapterMapper.selectOne(lambdaQueryWrapper);

        BookInfo bookInfo = bookInfoMapper.selectById(bookChapter.getBookId());
        BookInfoRespDto dto2 = BookInfoRespDto.builder().build();
        BeanUtils.copyProperties(bookInfo,dto2);

        ChapterInfoRespDto dto1 = ChapterInfoRespDto.builder()
                .id(bookChapter.getId())
                .bookId(bookChapter.getBookId())
                .chapterNum(bookChapter.getChapterNum())
                .chapterName(bookChapter.getChapterName())
                .chapterWordCount(bookChapter.getWordCount())
                .chapterUpdateTime(bookChapter .getUpdateTime())
                .isVip(bookChapter.getIsVip())
                .build();

        ChapterContentRespDto dto = ChapterContentRespDto.builder()
                .bookInfo(dto2)
                .chapterInfo(dto1)
                .id(bookContent1.getId())
                .chapterId(bookContent1.getChapterId())
                .bookContent(bookContent1.getContent())
                .createTime(String.valueOf(bookContent1.getCreateTime()))
                .updateTime(String.valueOf(bookContent1.getUpdateTime()))


                .build();


        Long chapterId1 = bookContent1.getChapterId();

        return RestResp.ok(dto.getChapterId());
    }
}
