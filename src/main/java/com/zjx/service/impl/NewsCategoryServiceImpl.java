package com.zjx.service.impl;

import com.zjx.po.NewsCategory;
import com.zjx.mapper.NewsCategoryMapper;
import com.zjx.service.INewsCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 新闻类别 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2024-02-11
 */
@Service
public class NewsCategoryServiceImpl extends ServiceImpl<NewsCategoryMapper, NewsCategory> implements INewsCategoryService {

}
