package com.zjx.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zjx.dto.HomeBookRespDto;
import com.zjx.mapper.BookInfoMapper;
import com.zjx.po.BookInfo;
import com.zjx.po.HomeBook;
import com.zjx.mapper.HomeBookMapper;
import com.zjx.service.IHomeBookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 小说推荐 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2024-01-29
 */
@Service
public class HomeBookServiceImpl extends ServiceImpl<HomeBookMapper, HomeBook> implements IHomeBookService {

    @Autowired
    private HomeBookMapper homeBookMapper;
    @Autowired
    private BookInfoMapper bookInfoMapper;

      @Override
       public List<HomeBookRespDto> books() {
           List<HomeBookRespDto> list = new ArrayList<>();
           List<HomeBook> homeBookList = homeBookMapper.selectList(null);
           homeBookList.forEach(homeBook -> {
               BookInfo bookInfo = bookInfoMapper.selectById(homeBook.getBookId());
               HomeBookRespDto dto = HomeBookRespDto.builder()
                       .type(homeBook.getType().intValue())   // byte - int
                       .bookId(homeBook.getBookId())
                       .picUrl(bookInfo.getPicUrl())
                       .bookName(bookInfo.getBookName())
                       .authorName(bookInfo.getAuthorName())
                       .bookDesc(bookInfo.getBookDesc())
                       .build();
               list.add(dto);
           });
           return list;
       }


    //stream流优化
    @Override
    public List<HomeBookRespDto> allBooks() {
        List<HomeBookRespDto> list = new ArrayList<>();
        List<HomeBook> homeBookList = homeBookMapper.selectList(null);
        // home_book是31条数据 对应一个book_id的集合  book_info表中100+条数据
//        List<Long> bookIdList = new ArrayList<>();
//        for (HomeBook homeBook : homeBookList) {
//            bookIdList.add(homeBook.getBookId());
//        }
//        homeBookList.forEach(homeBook -> {
//            bookIdList.add(homeBook.getBookId());
//        });

//        List<Long> bookIdList = homeBookList.stream().map(homeBook -> {
//            return homeBook.getBookId();
//        }).toList();
//        List<Long> bookIdList = homeBookList.stream().map(homeBook -> homeBook.getBookId()).toList();

        List<Long> bookIdList = homeBookList.stream().map(HomeBook::getBookId).toList();
        System.out.println(bookIdList);
        // in
        // select * from book_info where id  in ('1334318497132552192','1334318182169681920')
        LambdaQueryWrapper<BookInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(BookInfo::getId, bookIdList);

        // 当使用in查询数据时 本质上是对整个表进行扫描  扫描到一行数据时 判断是否符合in中的数据范围
        // 如果符合  就作为返回结果之一  所以返回的数据顺序是表中原本数据的顺序 而非in中的数据顺序
        //  List<Long>  ->  List<BookInfo> 不是按顺序一一对应的
        //  HomeBook  -> bookId ->  BookInfo 映射
        List<BookInfo> bookInfoList = bookInfoMapper.selectList(queryWrapper);

        Map<Long, BookInfo> bookInfoMap = new HashMap<>();
//        bookInfoList.forEach(bookInfo -> {
//            bookInfoMap.put(bookInfo.getId(),bookInfo);
//        });

        // key 「BookInfo::getId()」  value 「 t->t 」
//        Collectors.toMap(BookInfo::getId,t -> t);
        // 先处理数据 转化成map 再将转化后的结果收集起来 collect()
        Map<Long, BookInfo> collectedMap = bookInfoList.stream().collect(
                Collectors.toMap(BookInfo::getId, t -> t));
        System.out.println("==========");
//        System.out.println(collected);

        homeBookList.forEach(homeBook -> {
            // bookInfo  id -> homeBook bookId
//            BookInfo bookInfo = bookInfoMapper.selectById(homeBook.getBookId());
            BookInfo bookInfo = collectedMap.get(homeBook.getBookId());
            HomeBookRespDto dto = HomeBookRespDto.builder()
                    .type(homeBook.getType().intValue())   // byte - int
                    .bookId(homeBook.getBookId())
                    .picUrl(bookInfo.getPicUrl())
                    .bookName(bookInfo.getBookName())
                    .authorName(bookInfo.getAuthorName())
                    .bookDesc(bookInfo.getBookDesc())
                    .build();
            list.add(dto);
        });
        return list;
    }
}

