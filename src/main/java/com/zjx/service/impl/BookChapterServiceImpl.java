package com.zjx.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjx.dto.*;
import com.zjx.mapper.BookInfoMapper;
import com.zjx.mapper.HomeBookMapper;
import com.zjx.po.BookChapter;
import com.zjx.mapper.BookChapterMapper;
import com.zjx.po.BookInfo;
import com.zjx.po.HomeBook;
import com.zjx.service.IBookChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 小说章节 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2024-01-30
 */
@Service
public class BookChapterServiceImpl extends ServiceImpl<BookChapterMapper, BookChapter> implements IBookChapterService {


    @Autowired
    private BookChapterMapper bookChapterMapper;


    @Autowired
    private BookInfoMapper bookInfoMapper;
    @Autowired
    private HomeBookMapper homeBookMapper;

    @Override
    public List<ChapterInfoRespDto> chapterByid(Long bookId) {
        List<ChapterInfoRespDto> list = new ArrayList<>();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("book_id", bookId);
        List<BookChapter> bookChapterss = bookChapterMapper.selectList(queryWrapper);
        System.out.println(bookChapterss);

        for (BookChapter bookChapter : bookChapterss) {
            ChapterInfoRespDto dto = ChapterInfoRespDto.builder()
                    .id(bookChapter.getId())
                    .bookId(bookId)
                    .chapterNum(bookChapter.getChapterNum())
                    .chapterName(bookChapter.getChapterName())
                    .chapterWordCount(bookChapter.getWordCount())
                    .chapterUpdateTime(bookChapter.getUpdateTime())
                    .isVip(bookChapter.getIsVip())
                    .build();
            list.add(dto);
        }
        return list;
    }

    @Override
    public List<SearchBookWorkRespDto> searchWork(String workDirection) {
        List<SearchBookWorkRespDto> resultList = new ArrayList<>();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("work_direction", workDirection);
        List<BookInfoRespDto> list = bookInfoMapper.selectList(queryWrapper);
       /* for (BookInfoRespDto bookInfoRespDto : list) {
            System.out.println(bookInfoRespDto);
        }*/
        for (BookInfoRespDto bookInfoRespDto : list) {
            SearchBookWorkRespDto dto = SearchBookWorkRespDto.builder()
                    .id(bookInfoRespDto.getCategoryId())
                    .name(bookInfoRespDto.getCategoryName())
                    .build();
            //BeanUtils.copyProperties(searchBookWorkRespDto, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    @Override
    public List<SearchBookRespDto> searchBook(String keyword, String workDirection, String categoryId, String isVip, String bookStatus, String wordCountMin, String wordCountMax, String updateTimeMin, String sort, String pageNum, String pageSize) {

      /*  List<SearchBookRespDto> l1 = new ArrayList<>();
        QueryWrapper queryWrapper = new QueryWrapper();
        if (StringUtils.isNotBlank(keyword)) {
            queryWrapper.like("book_name", keyword);
            List<SearchBookRespDto> list = bookChapterMapper.selectList(queryWrapper);
            for (SearchBookRespDto searchBookRespDto : list) {
                SearchBookRespDto dto = SearchBookRespDto.builder().build();
                BeanUtils.copyProperties(list,dto);
                l1.add(dto);
            }
            return l1;
        }*/


        List<SearchBookRespDto> resultList = new ArrayList<>();
        QueryWrapper<BookChapter> queryWrapper = new QueryWrapper<>();

        // 添加搜索条件
        if (StringUtils.isNotBlank(keyword)) {
            queryWrapper.like("book_name", keyword);
        }
        if (StringUtils.isNotBlank(workDirection)) {
            queryWrapper.eq("work_direction", workDirection);
        }
        if (StringUtils.isNotBlank(categoryId)) {
            queryWrapper.eq("category_id", categoryId);
        }
        if (StringUtils.isNotBlank(isVip)) {
            queryWrapper.eq("is_vip", isVip);
        }
        if (StringUtils.isNotBlank(bookStatus)) {
            queryWrapper.eq("book_status", bookStatus);
        }
        if (wordCountMin != null) {
            queryWrapper.ge("word_count", wordCountMin);
        }
        if (wordCountMax != null) {
            queryWrapper.le("word_count", wordCountMax);
        }
        if (updateTimeMin != null) {
            queryWrapper.ge("update_time", updateTimeMin);
        }
        if (StringUtils.isNotBlank(sort)) {
            // 根据sort参数设置排序字段，这里假设sort参数的值是数据库字段名
            queryWrapper.orderByAsc(sort);
        }

        // 分页查询
        Page<BookChapter> page = new Page<>();
        IPage<BookChapter> bookChapterIPage = bookChapterMapper.selectPage(page, queryWrapper);
        List<BookChapter> bookChapterList = bookChapterIPage.getRecords();
        System.out.println(bookChapterList);
        // 将查询结果转换为SearchBookRespDto列表
        for (BookChapter bookChapter : bookChapterList) {
            SearchBookRespDto dto = SearchBookRespDto.builder().build();
            BeanUtils.copyProperties(bookChapter, dto);
            resultList.add(dto);
        }

        return resultList;
    }

/*    @Override
    public List<ChapterInfoRespDto> chapterByid(Long bookId) {
        List<HomeBookRespDto> list = new ArrayList<>();
        List<HomeBook> homeBookList = homeBookMapper.selectList(null);

        // Convert HomeBook list to a list of book IDs
        List<Long> bookIdList = homeBookList.stream().map(HomeBook::getBookId).toList();

        // Query BookInfo using the list of book IDs
        Map<Long, BookInfo> bookInfoMap = bookInfoMapper.selectList(new QueryWrapper<BookInfo>().in("id", bookIdList)).stream()
                .collect(Collectors.toMap(BookInfo::getId, Function.identity()));

        // Convert HomeBook list to HomeBookRespDto list
        homeBookList.forEach(homeBook -> {
           // Long bookId = homeBook.getBookId();
            BookInfo bookInfo = bookInfoMap.get(bookId);
            if (bookInfo != null) {
                list.add(HomeBookRespDto.builder()
                        .type(homeBook.getType().intValue())   // byte - int
                        .bookId(homeBook.getBookId())
                        .picUrl(bookInfo.getPicUrl())
                        .bookName(bookInfo.getBookName())
                        .authorName(bookInfo.getAuthorName())
                        .bookDesc(bookInfo.getBookDesc())
                        .build());
            }
        });
        return list;
    }*/


}

