package com.zjx.service;

import com.zjx.dto.BookInfoRespDto;
import com.zjx.dto.HomeBookRespDto;
import com.zjx.dto.LastChapterRespDto;
import com.zjx.dto.RankBookRespDto;
import com.zjx.po.BookInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 小说信息 服务类
 * </p>
 *
 * @author zjx
 * @since 2024-01-29
 */
public interface IBookInfoService extends IService<BookInfo> {
   BookInfoRespDto  bookInfoResp(@PathVariable("id") Long id);
   LastChapterRespDto look(Long bookld);

   List<BookInfoRespDto> bookRecResp(@RequestParam("bookId") Long bookId);


   List<RankBookRespDto> visit();
   List<RankBookRespDto> newest();

   List<RankBookRespDto> updaterank();




}
