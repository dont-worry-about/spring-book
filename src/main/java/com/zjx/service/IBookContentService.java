package com.zjx.service;

import com.zjx.dto.ChapterContentRespDto;
import com.zjx.po.BookContent;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zjx.resp.RestResp;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * <p>
 * 小说内容 服务类
 * </p>
 *
 * @author zjx
 * @since 2024-01-31
 */
public interface IBookContentService extends IService<BookContent> {
    ChapterContentRespDto content(@PathVariable("chapterId") Long chapterId);

    RestResp<Long> contentNext(@Param("chapterId") Long chapterId);

    RestResp<Long> contentPre(@Param("chapterId") Long chapterId);

}
