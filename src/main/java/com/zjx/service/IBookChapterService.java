package com.zjx.service;

import com.zjx.dto.ChapterInfoRespDto;
import com.zjx.dto.SearchBookRespDto;
import com.zjx.dto.SearchBookWorkRespDto;
import com.zjx.po.BookChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 小说章节 服务类
 * </p>
 *
 * @author zjx
 * @since 2024-01-30
 */
public interface IBookChapterService extends IService<BookChapter> {
    List<ChapterInfoRespDto> chapterByid(@PathVariable("bookId") Long bookId);
    /* List<ChapterInfoRespDto> allChapterByid(@PathVariable("bookId") Long bookId);*/

    List<SearchBookWorkRespDto> searchWork(@RequestParam("workDirection") String workDirection);

    List<SearchBookRespDto> searchBook(@Param("keyword") String keyword,
                                       @Param("workDirection") String workDirection,
                                       @Param("categoryId") String categoryId,
                                       @Param("isVip") String isVip,
                                       @Param("bookStatus") String bookStatus,
                                       @Param("wordCountMin") String wordCountMin,
                                       @Param("wordCountMax") String wordCountMax,
                                       @Param("updateTimeMin") String updateTimeMin,
                                       @Param("sort") String sort,
                                       @Param("pageNum") String pageNum,
                                       @Param("pageSize") String pageSize);
}
