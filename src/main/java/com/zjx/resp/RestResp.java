package com.zjx.resp;

import lombok.Data;
import lombok.Getter;

/*@Getter*/
@Data
public class RestResp<T> {
    // 成功对应 00000
    private String code;
    // 处理失败的具体信息
    private String message;
    // 处理成功的数据
    private T data;
    private boolean ok = true;

    private RestResp(T data) {
        this.code = "00000";
        this.message = "成功";
        this.data = data;
    }

    public static <T> RestResp<T> ok(T data) {
        return new RestResp(data);
    }

    public static <T> RestResp<T> fail(String code, String message, T data) {
        return new RestResp(code, message);
    }

    private RestResp(String code, String message) {
        this.code = code;
        this.message = message;
        this.ok = false;
    }
}
