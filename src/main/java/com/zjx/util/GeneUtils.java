package com.zjx.util;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.sql.Types;
import java.util.Collections;
public class GeneUtils {

    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/noviesplus?useUnicode=true&characterEncoding=utf-8&allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=Asia/Shanghai",
                        "root", "123456")
                .globalConfig(builder -> {
                    builder.author("zjx") // 设置作者
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("src\\main\\java\\"); // 指定输出目录
                })
                .dataSourceConfig(builder ->
                        builder.typeConvertHandler((globalConfig, typeRegistry, metaInfo) -> {
                            int typeCode = metaInfo.getJdbcType().TYPE_CODE;
                            if (typeCode == Types.SMALLINT) {
                                // 自定义类型转换
                                return DbColumnType.INTEGER;
                            }
                            return typeRegistry.getColumnType(metaInfo);

                        }))
                .packageConfig(builder -> {
                    builder.parent("com.zjx") // 设置父包名
                            .moduleName("") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(
                                    OutputFile.xml, "src/main/resources/mapper")) // 设置mapperXml生成路径
                            .entity("po")
                            .service("service")
                            .serviceImpl("service.impl")
                            .mapper("mapper")
                            .xml("mapper.xml")
                            .controller("controller")
                            //                            .other("other")
                            //                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "D://"))
                            .build();

                })
                .strategyConfig(builder -> {
                    // sys_user  t_user   User  UserMapper  UserService
                    builder.addInclude("home_friend_link"); // 设置需要生成的表名
                    //                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
