package com.zjx.util;

import org.springframework.data.util.Pair;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.generator.RandomGenerator;

public class CaptchaUtils {

    public static Pair<String, String> getImageBase64() {
        // 生成四位数字的验证码  宽高是100*38
        RandomGenerator randomGenerator = new RandomGenerator("0123456789", 4);
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(100, 38);
        lineCaptcha.setGenerator(randomGenerator);

        String code = lineCaptcha.getCode();
        String imageBase64 = lineCaptcha.getImageBase64();
        System.out.println("--------------");
        System.out.println(code);
        System.out.println(imageBase64);
        System.out.println("--------------");
        // 有一对数据需要返回 可以使用pair 第一个数据叫first 第二个数据叫second
        Pair<String, String> pair = Pair.of(code, imageBase64);
        return pair;
    }
}