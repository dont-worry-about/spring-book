package com.zjx.util;

import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.epub.EpubReader;

import java.io.File;
import java.io.FileInputStream;

public class EpubUtils {

    public static void main(String[] args) throws Exception {
        // IO处理的相对路径  是项目的根路径
        File file = new File("epub/lizhi.epub");
        System.out.println(file.exists());
        FileInputStream fis = new FileInputStream(file);
        // 是通过将输入流传入epubreader  来获取它所识别出的book对象
        EpubReader epubReader = new EpubReader();
        Book book = epubReader.readEpub(fis);
        System.out.println();
    }
}