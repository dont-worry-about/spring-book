package com.zjx.mapper;

import com.zjx.po.HomeFriendLink;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 友情链接 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2024-02-17
 */
@Mapper
public interface HomeFriendLinkMapper extends BaseMapper<HomeFriendLink> {

}
