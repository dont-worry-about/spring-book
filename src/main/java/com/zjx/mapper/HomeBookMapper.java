package com.zjx.mapper;

import com.zjx.po.HomeBook;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 小说推荐 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2024-01-29
 */
@Mapper
public interface HomeBookMapper extends BaseMapper<HomeBook> {

}
