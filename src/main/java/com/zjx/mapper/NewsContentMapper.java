package com.zjx.mapper;

import com.zjx.po.NewsContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 新闻内容 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2024-02-11
 */
@Mapper
public interface NewsContentMapper extends BaseMapper<NewsContent> {

}
