package com.zjx.mapper;

import com.zjx.po.NewsCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 新闻类别 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2024-02-11
 */
@Mapper
public interface NewsCategoryMapper extends BaseMapper<NewsCategory> {

}
