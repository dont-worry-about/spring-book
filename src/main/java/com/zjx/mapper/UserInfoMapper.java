package com.zjx.mapper;

import com.zjx.po.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户信息 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2024-02-01
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
