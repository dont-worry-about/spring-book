package com.zjx.mapper;

import com.zjx.po.BookContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 小说内容 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2024-01-31
 */
@Mapper
public interface BookContentMapper extends BaseMapper<BookContent> {

}
