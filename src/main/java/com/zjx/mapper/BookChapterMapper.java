package com.zjx.mapper;

import com.zjx.po.BookChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 小说章节 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2024-01-30
 */
@Mapper
public interface BookChapterMapper extends BaseMapper<BookChapter> {

}
