package com.zjx.mapper;

import com.zjx.po.BookInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 小说信息 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2024-01-29
 */
@Mapper
public interface BookInfoMapper extends BaseMapper<BookInfo> {
    @Update("UPDATE book_info SET visit_count = visit_count + 1 WHERE id = #{bookId}")
    void updateClickCount(@Param("bookId") String bookId);
}
