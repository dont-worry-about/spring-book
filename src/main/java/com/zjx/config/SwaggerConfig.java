package com.zjx.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI openAPI(){
        OpenAPI api = new OpenAPI();
        api.info(new Info().title("阅读类门户网站")
                .description("基于springboot实现")
                .version("v1.0.0"));
        return api;
    }
}