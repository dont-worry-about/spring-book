package com.zjx.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LastChapterRespDto {

    private ChapterInfoRespDto chapterInfo;
    private Integer chapterTotal;
    private String contenSummary;
}