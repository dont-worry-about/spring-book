package com.zjx.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SearchBookRespDto {
        private Integer pageNum;
        private Integer pageSize;
        private Integer total;
        private List<BookInfoRespDto> list;
        private Integer pages;
}
