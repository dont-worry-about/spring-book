package com.zjx.dto;


import lombok.Builder;
import lombok.Data;

//在处理前端和后端进行请求和响应的对象
@Data
@Builder
public class HomeBookRespDto {

        private Integer type;

        private Long bookId;

        private String picUrl;

        private String bookName;

        private String authorName;

        private String bookDesc;
}

