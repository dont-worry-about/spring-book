package com.zjx.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SearchBookWorkRespDto {
    private Long id;
    private String name;
}
