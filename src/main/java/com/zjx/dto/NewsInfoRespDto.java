package com.zjx.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NewsInfoRespDto {

    private Integer id;
    private Integer categoryId;
    private String categoryName;
    private String sourceName;
    private String title;
    private String updateTime;
    private String content;

}
