package com.zjx.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VerifyCodeRespDto {

    @Schema(description = "sessionId")
    private String sessionId;
    @Schema(description = "Base64 编码的验证码图片")
    private String img;
}