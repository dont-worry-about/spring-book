package com.zjx.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChapterContentRespDto {
    private BookInfoRespDto bookInfo;
    private ChapterInfoRespDto chapterInfo;
    private Long id;
    private Long chapterId;
    private String bookContent;
    private String createTime;
    private String updateTime;
}
