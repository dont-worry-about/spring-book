package com.zjx.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class ChapterInfoRespDto {
    private Long id;
    private Long bookId;
    private Integer chapterNum;
    private String chapterName;
    private Integer chapterWordCount;
    private LocalDateTime chapterUpdateTime;
    private Integer isVip;

}
