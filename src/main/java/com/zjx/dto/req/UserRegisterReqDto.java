package com.zjx.dto.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRegisterReqDto {

    private String username;
    private String password;
    private String velCode;
    private String sessionId;
    private String imgVerifyCode;
}
