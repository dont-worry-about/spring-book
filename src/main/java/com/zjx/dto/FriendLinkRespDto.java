package com.zjx.dto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FriendLinkRespDto {
    private String linkName;
    private String linkUrl;
}
